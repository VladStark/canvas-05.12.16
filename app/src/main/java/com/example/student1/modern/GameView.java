package com.example.student1.modern;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;


public class GameView extends View {

    private int size,x,y,yspeed,xspeed;
    private float touchX,touchY;
    private Bitmap pic;
    public GameView (Context context) {
        super(context);
        pic = BitmapFactory.decodeResource(context.getResources(),R.drawable.cat);
        size = 100;
        xspeed = 5;
        yspeed = 5;
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Rect source = new Rect(0,0,pic.getWidth(),pic.getHeight());
        Rect rect = new Rect(x - size,y - size,x + size,y + size);
        canvas.drawBitmap(pic,source,rect,new Paint());
        x += xspeed;
        y += yspeed;

        if (x > canvas.getWidth()  || x < 0) xspeed = -xspeed;
        if (y > canvas.getHeight() || y < 0) yspeed = -yspeed;

        invalidate();

    }

    public void setTouchCoords(float x, float y) {
        float tx = this.x;
        float ty = this.y;
        if (tx > x - size && tx < x + size && ty > y - size && ty < y + size) {
            size +=5;
        }
        invalidate();
    }

}
